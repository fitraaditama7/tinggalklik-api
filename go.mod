module gitlab.com/fitraaditama7/tinggalklik-api.git

go 1.14

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/lib/pq v1.8.0 // indirect
	github.com/sirupsen/logrus v1.6.0 // indirect
	github.com/spf13/viper v1.7.1 // indirect
)

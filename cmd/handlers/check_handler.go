package handlers

import (
	"context"
	"log"
	"strconv"

	"gitlab.com/fitraaditama7/tinggalklik-api.git/cmd/lib"

	"gitlab.com/fitraaditama7/tinggalklik-api.git/cmd/responses"

	"github.com/labstack/echo"
	"gitlab.com/fitraaditama7/tinggalklik-api.git/cmd/config"
	"gitlab.com/fitraaditama7/tinggalklik-api.git/cmd/services"
)

type checkHandler struct {
	service services.CheckService
}

var timeout = config.TIMEOUT

func New(e *echo.Echo, service services.CheckService) {
	handler := checkHandler{
		service: service,
	}

	e.GET("/check/:id", handler.GetByID)
	e.POST("/check", handler.Insert)
	e.PUT("/check/:id", handler.Update)
	e.DELETE("/check/:id", handler.Delete)
	e.GET("/check", handler.List)
}

func (r *checkHandler) GetByID(c echo.Context) error {
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		log.Fatal(err)
		return responses.WithError(&c, "ID Format Error", lib.ErrInvalidRequestForm)
	}

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	data, err := r.service.GetByID(ctx, id)
	if err != nil {
		log.Fatal(err)
		return responses.WithError(&c, "Internal Server Error", lib.ErrInternalServerError)
	}

	return responses.WithData(&c, 200, "Success", data)
}

func (r *checkHandler) Insert(c echo.Context) error {
	return nil
}

func (r *checkHandler) Update(c echo.Context) error {
	return nil
}

func (r *checkHandler) Delete(c echo.Context) error {
	return nil
}

func (r *checkHandler) List(c echo.Context) error {
	return nil
}

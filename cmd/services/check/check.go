package check

import (
	"context"
	"time"

	"gitlab.com/fitraaditama7/tinggalklik-api.git/cmd/entities"
	"gitlab.com/fitraaditama7/tinggalklik-api.git/cmd/repositories"
	"gitlab.com/fitraaditama7/tinggalklik-api.git/cmd/services"
)

type checkService struct {
	checkRepo      repositories.CheckRepository
	contextTimeout time.Duration
}

func New(checkRepository repositories.CheckRepository, timeout time.Duration) services.CheckService {
	return &checkService{
		checkRepo:      checkRepository,
		contextTimeout: timeout,
	}
}

func (m *checkService) GetByID(ctx context.Context, id int64) (e *entities.Check, err error) {
	return nil, nil
}

func (m *checkService) Insert(ctx context.Context, e entities.Check) (*entities.Check, error) {
	return nil, nil
}

func (m *checkService) Update(ctx context.Context, e entities.Check) (*entities.Check, error) {
	return nil, nil
}

func (m *checkService) Delete(ctx context.Context, e entities.Check) (*entities.Check, error) {
	return nil, nil
}

func (m *checkService) List(ctx context.Context, page int64, size int64) ([]*entities.Check, error) {
	return nil, nil
}

package config

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/joho/godotenv"
)

var (
	PORT      = 0
	SECRETKEY []byte
	DBDRIVER  = ""
	DBURL     = ""
	TIMEOUT   time.Duration
)

// Load Configuration from env
func Load() {
	var err error
	err = godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}

	PORT, err = strconv.Atoi(os.Getenv("API_PORT"))
	if err != nil {
		PORT = 4000
	}

	DBHOST := os.Getenv("DATABASE_HOST")
	if DBHOST == "" {
		DBHOST = "127.0.0.1"
	}

	DBDRIVER = os.Getenv("DB_DRIVER")

	DBURL = fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?charset=utf8&parseTime=1&loc=Asia/Jakarta",
		os.Getenv("DATABASE_NAME"), os.Getenv("DATABASE_PASSWORD"), DBHOST, os.Getenv("DATABASE_NAME"))
	SECRETKEY = []byte(os.Getenv("API_SECRET"))

	timeout, err := strconv.ParseInt(os.Getenv("TIMEOUT"), 10, 64)
	if err != nil {
		log.Fatal(err)
	}
	TIMEOUT = time.Duration(timeout) * time.Second
}

package check

import (
	"context"
	"database/sql"

	"gitlab.com/fitraaditama7/tinggalklik-api.git/cmd/entities"
	"gitlab.com/fitraaditama7/tinggalklik-api.git/cmd/repositories"
)

type mySQL struct {
	db *sql.DB
}

func New(db *sql.DB) repositories.CheckRepository {
	return &mySQL{db}
}

func (m *mySQL) GetByID(ctx context.Context, id int64) (e *entities.Check, err error) {
	return nil, nil
}

func (m *mySQL) Insert(ctx context.Context, e entities.Check) (*entities.Check, error) {
	return nil, nil
}

func (m *mySQL) Update(ctx context.Context, e entities.Check) (*entities.Check, error) {
	return nil, nil
}

func (m *mySQL) Delete(ctx context.Context, e entities.Check) (*entities.Check, error) {
	return nil, nil
}

func (m *mySQL) List(ctx context.Context, page int64, size int64) ([]*entities.Check, error) {
	return nil, nil
}

package repositories

import (
	"context"

	"gitlab.com/fitraaditama7/tinggalklik-api.git/cmd/entities"
)

type CheckRepository interface {
	GetByID(ctx context.Context, id int64) (e *entities.Check, err error)
	Insert(ctx context.Context, e entities.Check) (*entities.Check, error)
	Update(ctx context.Context, e entities.Check) (*entities.Check, error)
	Delete(ctx context.Context, e entities.Check) (*entities.Check, error)
	List(ctx context.Context, page int64, size int64) ([]*entities.Check, error)
}

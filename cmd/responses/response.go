package responses

import (
	"encoding/json"

	"github.com/labstack/echo"
	"gitlab.com/fitraaditama7/tinggalklik-api.git/cmd/lib"
)

type res struct {
	Error   bool        `json:"error"`
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// WithData is function write response data
func WithData(w *echo.Context, code int, msg string, payload interface{}) error {
	c := *w

	var result = res{
		Code:    code,
		Error:   false,
		Message: msg,
		Data:    payload,
	}
	responses, _ := json.Marshal(result)
	c.Response().Header().Set("Content-Type", "application/json")
	c.Response().WriteHeader(result.Code)
	c.Response().Write(responses)
	return nil
}

// WithError is function to write error
func WithError(w *echo.Context, msg string, err lib.CustomError) error {
	c := *w
	var result = res{
		Code:    err.Code,
		Error:   true,
		Message: err.Message,
		Data:    msg,
	}

	responses, _ := json.Marshal(result)
	c.Response().Header().Set("Content-Type", "application/json")
	c.Response().WriteHeader(result.Code)
	c.Response().Write(responses)
	return nil
}

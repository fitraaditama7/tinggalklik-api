package databases

import (
	"database/sql"
	"log"
	"os"

	"gitlab.com/fitraaditama7/tinggalklik-api.git/cmd/config"
)

// LoadDB Load Database
func LoadDB() *sql.DB {
	db, err := sql.Open(config.DBDRIVER, config.DBURL)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	return db
}

package lib

type CustomError struct {
	Code    int
	Message string
}

var (
	ErrInvalidRequestForm = CustomError{
		Code:    400,
		Message: "BAD_REQUEST",
	}
	ErrEntityNotFound = CustomError{
		Code:    404,
		Message: "DATA_NOT_FOUND",
	}
	ErrInternalServerError = CustomError{
		Code:    500,
		Message: "INTERNAL_SERVER_ERROR",
	}
)

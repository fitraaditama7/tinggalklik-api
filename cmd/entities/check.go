package entities

type Check struct {
	ID       int64
	Name     string
	IsActive bool
}
